# Project: Cows Identification System (CowsID)

This project aimed to develop a face recognition system for dairy farm cows using advanced deep learning models and computer vision techniques.

The project includes of three sub-repositories (Private and only accessible by project members):
1. Development codes: [Gitlab CowsID](https://gitlab.unimelb.edu.au/hodach/cows_id)
2. Deployment codes: [Gitlab CowsID Jetson](https://gitlab.unimelb.edu.au/hodach/cows_id_jetson)
3. Data: [Gitlab CowsID Data](https://gitlab.unimelb.edu.au/hodach/cows_id_data)

# References
Link to the Digital Agriculture, Food and Wine Research Group. The University of Melbourne: [FVAS-DAFW](https://fvas.unimelb.edu.au/research/groups/digital-agriculture-food-and-wine)
<br>
\#digitalagriculture \#computervision \#machinelearning \#biometric
